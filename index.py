
def myFunction(number):

    result = str(number)

    if(number % 3 == 0):
        result = 'fizz'

    if(number % 5 == 0):
        result = 'buzz'

    if(number % 3 == 0 and number % 5 == 0):
        result = 'fizzbuzz'

    return result

if __name__ == '__main__':
    number = int(input("Entrez un nombre : "))
    print(myFunction(number))