import index
import unittest

class TestStringMethods(unittest.TestCase):

    def test_multpiple_by_3(self):
        number = index.myFunction(9)
        self.assertEqual(number, "fizz", "Ce nombre n'est pas un multiple de 3")

    def test_multpiple_by_5(self):
        number = index.myFunction(25)
        self.assertEqual(number, "buzz", "Ce nombre n'est pas un multiple de 5")

    def test_multpiple_by_3_and_5(self):
        number = index.myFunction(15)
        self.assertEqual(number, "fizzbuzz", "Ce nombre n'est pas un multiple de 3 et de 5")

    def test_not_multpiple_by_3_or_by_5(self):
        number = index.myFunction(16)
        self.assertEqual(number, "16", "Ce nombre est un multiple de 3 ou de 5")

if __name__ == '__main__':
    unittest.main()