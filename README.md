# FizzBuzz

FizzBuzz est un petit projet Python qui implémente une fonction pour convertir un nombre en une chaîne de caractères selon certaines règles. Il est également livré avec des tests unitaires pour garantir le bon fonctionnement du code.

## Fonctionnalités

La fonction `fizzBuzz` prend en entrée un nombre entier et renvoie une chaîne de caractères en fonction des règles suivantes :

- Pour les multiples de 3, la fonction retourne "Fizz".
- Pour les multiples de 5, la fonction retourne "Buzz".
- Pour les multiples de 3 et de 5, la fonction retourne "FizzBuzz".

## Structure du Projet

Le projet est organisé comme suit :

- `fizzbuzz.py` : Ce fichier contient la fonction `fizzBuzz` que vous pouvez utiliser pour appliquer les règles de FizzBuzz.
- `test_fizzbuzz.py` : Ce fichier contient des tests unitaires pour vérifier la validité de la fonction `fizzBuzz`.
- `.gitlab-ci.yml` : Ce fichier de configuration contient les étapes nécessaires pour automatiser les tests dans une pipeline Gitlab.

## Installation

Avant d'exécuter les tests ou d'utiliser la fonction `fizzBuzz`, assurez-vous d'avoir Python installé sur votre système.

## Exécution des Tests

Vous pouvez exécuter les tests unitaires en utilisant le module unittest. Utilisez la commande suivante pour exécuter les tests :

```bash

python -m unittest testIndex.py
```
